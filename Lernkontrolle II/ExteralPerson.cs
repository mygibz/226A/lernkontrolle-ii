﻿using System;

namespace Lernkontrolle_II {
    class ExteralPerson : Person {
        public string Company;

        public ExteralPerson(string firstName, string lastName, DateTime birthday, string company, Gender gender = Gender.Other) : base(firstName, lastName, birthday, gender) {
            Company = company;
        }

        public new void Print() {
            base.Print();
            System.Console.WriteLine("Company: " + Company);
        }
    }
}