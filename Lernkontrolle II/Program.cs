﻿using System;

namespace Lernkontrolle_II {
    class Program {
        static void Main(string[] args) {
            // create person
            InternalPerson person = new InternalPerson("Yanik", "Ammann", DateTime.Parse("2003-07-03"), "Healthcare", DateTime.Now, Gender.Male);
            person.Print();

            Console.WriteLine("\n");

            // add two emails, two phoneNumbers and a postal address
            person.AddContactData(new EmailAddress("confused@ant.lgbt"));
            person.AddContactData(new EmailAddress("info@1yanik3.com"));
            person.AddContactData(new PhoneNumber("041 750 13 11", PhoneNumberType.Landline));
            person.AddContactData(new PhoneNumber("076 802 24 48", PhoneNumberType.Mobile));
            person.AddContactData(new PostalAddress("Bruunemattstrasse 20", 6314, "Unterägeri"));
            person.Print();

            Console.WriteLine("\n");

            // change primary email
            person.SetPrimaryEmailAddress(new EmailAddress("info@1yanik3.com"));
            person.Print();

            Console.WriteLine("\n");

            // change primary phoneNumber
            person.SetPrimaryPhoneNumber(new PhoneNumber("076 802 24 48")); // constructor assumes that, if no type is present, Mobile is meant
            person.Print();

            Console.WriteLine("\n");

            // change primary email to one that does not exist
            person.SetPrimaryEmailAddress(new EmailAddress("me@ant.lgbt"));
        }
    }
}
