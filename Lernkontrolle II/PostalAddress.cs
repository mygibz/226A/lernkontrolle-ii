﻿namespace Lernkontrolle_II {
    class PostalAddress : ContactData {
        public string Street;
        public int ZipCode;
        public string City;

        public PostalAddress(string street, int zipcode, string city) {
            Street = street;
            ZipCode = zipcode;
            City = city;
        }

        /// <summary>
        /// Adds data to the Print function that's already defined
        /// </summary>
        public override void Print() {
            //base.Print();
            System.Console.WriteLine("Address: " + Street);
            System.Console.WriteLine($"         {ZipCode}, {City}");
        }

    }
}