﻿using System;

namespace Lernkontrolle_II {
    class InternalPerson : Person {
        public string Department;
        public DateTime EntryDate;

        public InternalPerson(string firstName, string lastName, DateTime birthday, string department, DateTime entryDate, Gender gender = Gender.Other) : base(firstName, lastName, birthday, gender) {
            Department = department;
            EntryDate = entryDate;
        }

        public new void Print() {
            base.Print();
            Console.WriteLine("Department: " + Department);
            Console.WriteLine("EntryDate: " + EntryDate.ToLongDateString());
        }
    }
}