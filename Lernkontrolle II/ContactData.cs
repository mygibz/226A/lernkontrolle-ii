﻿using System;

namespace Lernkontrolle_II {
    class ContactData {

        /// <summary>
        /// empty function that allows children to overwrite
        /// </summary>
        public virtual void Print() {
            
        }
    }
}