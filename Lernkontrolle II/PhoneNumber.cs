﻿namespace Lernkontrolle_II {
    class PhoneNumber : ContactData {
        public string Number;
        public PhoneNumberType Type;

        public PhoneNumber(string number, PhoneNumberType type = PhoneNumberType.Mobile) {
            Number = number;
            Type = type;
        }

        /// <summary>
        /// Adds data to the Print function that's already defined
        /// </summary>
        public override void Print() {
            //base.Print();
            System.Console.WriteLine($"Phone Number: {Number} ({Type})");
        }
    }
}