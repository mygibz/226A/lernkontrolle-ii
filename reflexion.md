# M226A: Lernkontrolle Teil II - Reflexion

Das fertige Programm wurde als ZIP an das Assignement angehängt, ansonsten kann es [hier](https://gitlab.com/ConfusedAnt/demo-contact-data-management) eingesehen werden.

## Woran ist die Vererbung erkennbar?

Die Klassen basieren aufeinander. Dies kann man besonders bei den Personen beachten. z.B. Übernimmt die Klasse *Internal-Person* einen grossen Teil des Konstruktors der Klasse *Person*, oder auch kann eine einzelne Funktion, alle verschidenen *ContactData-Children* abdecken, ohne, dass man verschidene funktionen machen muss.

## Vorteil/Nutzen

Anstatt immer alles neu zu schreiben, kann man einfach z.B. den Konstruktor der übergeordneten Klasse verwenden und diesen entweder erweitern oder überschreiben.
Es macht den Code auch einfacher zu lesen.

## Benötigte zeit

Ungefähr 4 Stunden. Einige dieser Zeit wurde dafür verwendet, um das Klassendiagramm zu verstehen. Weitere 20 bis 30 Minuten für diese Reflexion.